import Vue from 'vue'
import Vuex from 'vuex'

// import module from './modules/module'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    // module
  },
  strict: debug
})
