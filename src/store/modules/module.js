// import _ from 'lodash'

// import * as types from '../mutation-types'
// import { mapSetAction, mapSetMutations, mutationSetStatus, mutationSave } from '../utils'
// import api from '@/api/module'

// const state = {
//   _list: null,
//   list: {},
// }

// const getters = {
//   listStatus: state => state._list,
//   list: state => state.list,
// }

// const actions = {
//   getList: (context, { options = {} }) => {
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'MODULE_GET_ACTION',
//       name: ['list'],
//       status: ['_list'],
//       call: api.getList,
//       params: [],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           commit(successMutation, params)
//           commit(types.MODULE_SAVE, params)
//         },
//         ...options
//       }
//     })
//   }
// }

// const mutations = {
//   ...mapSetMutations({ types, mutation: 'MODULE_GET_ACTION' }),
//
//   ...mutationSave(types.MODULE_SAVE),
//   ...mutationSetStatus(types.MODULE_RESET_STATUS, null)
// }

// export default {
//   namespaced: true,
//   state,
//   getters,
//   actions,
//   mutations
// }
