import Vue from 'vue'
import _ from 'lodash'

import App from './App'
import router from './router'
import store from './store'
import config from './config'

import VTooltip from 'v-tooltip'

import InlineEditing, { i3StoreModules } from 'y3ti-i3'

import Routing, { r3StoreModules } from 'y3ti-r3'

Vue.use(Routing, {
  store,
  router,
  config
})

Vue.use(InlineEditing, {
  store,
  router,
  config
})

_.each(_.merge({}, i3StoreModules, r3StoreModules), (module, name) => {
  store.registerModule(name, module)
})

Vue.use(VTooltip)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
