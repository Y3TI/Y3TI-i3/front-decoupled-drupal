// ! \\ If not overrided in prod or test config, a dev settings will be set for all environments!

import CustomLayout from '@/components/Layouts/CustomLayout'
import DefaultLayout from '@/components/Layouts/DefaultLayout'

import MenuBlock from '@/components/Blocks/MenuBlock'

import DefaultComponent from '@/components/Default'
import PageComponent from '@/components/Page'

import PageCustomPreset from '@/node-presets/page/custom'

const ghostZones = (node) => {
  return {
    page: { title: 'Default Settings', register: [{ node, path: ['title'] }] }
  }
}

export default {
  TITLE: 'Decoupled Drupal',
  API_URL: 'http://decoupled-drupal.wip/api',
  QUERY_MERGE: { timestamp: Date.now() },

  EDITABLE_FIELDS_GROUP_CONFIG (props) {
    let groupOptions = {
      // beforeAll: [ 'my-new-widget' ],
      // afterAll: [ 'my-new-widget' ]
    }
    // if (config.id === 'accordion') {
    //   groupOptions.hideAll = true
    // }

    return groupOptions
  },
  EDITABLE_FIELD_CONFIG (props) {
    let fieldOptions = {
      // beforeField: {},
      // afterField: {}
    }
    // if (config.reference === 'media' && fieldConfig.type === 'image') {
    //   fieldOptions = {
    //     beforeField: [ 'my-new-widget' ]
    //     // hideField: true
    //     // afterField: [ 'my-new-widget' ]
    //   }
    // }

    // if (fieldConfig.id === 'field_pg_rows') {
    //   fieldOptions.hideField = true
    // }

    return fieldOptions
  },
  EDITABLE_WIDGETS_MAPPING (props) {
    return false
  },
  EDITABLE_SETTINGS_FIELD_CONFIG (props) {
    let fieldOptions = {
      // beforeField: {},
      // afterField: {}
    }

    // if (fieldConfig.type === 'checkbox') {
    //   fieldOptions = {
    //     beforeField: [ 'my-new-widget' ]
    //     // hideField: true
    //     // afterField: [ 'my-new-widget' ]
    //   }
    // }

    return fieldOptions
  },
  EDITABLE_SETTINGS_FIELDS_MAPPING (props) {
    return false
  },

  EDITABLE_NODE_PRESETS: {
    page: {
      custom: { name: 'Custom', data: PageCustomPreset }
    }
  },
  EDITABLE_GHOST_ZONES (node) {
    return ghostZones(node)
  },

  EDITABLE_NEW_NODE_COMPONENTS: {
    page: PageComponent
  },

  EDITABLE_CUSTOM_WIDGETS: {
    // 'my-new-widget': () => import('@/components/Editable-Custom/MyNewWidget')
  },

  ROUTING: {
    layoutsMapping: {
      default: {
        path: '',
        component: DefaultLayout,
        children: [{
          path: '',
          components: {
            'menu': MenuBlock,
            'main-content': { template: '<router-view/>' }
          },
          children: []
        }]
      },
      custom: {
        path: '',
        component: CustomLayout,
        children: [{
          path: '',
          components: {
            'menu': MenuBlock,
            'main-content': { template: '<router-view/>' }
          },
          children: []
        }]
      }
    },

    componentsMapping: {
      default: DefaultComponent,
      page: PageComponent
    }
  }
}
